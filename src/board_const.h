/*--------------------------------------------------------------------------*/
/* game board constants                                                     */
/*--------------------------------------------------------------------------*/

#ifndef __MY_BOARD_CONST_H
#define __MY_BOARD_CONST_H

/*--------------------------------------------------------------------------*/
/* defines                                                                  */
/*--------------------------------------------------------------------------*/

#define BOARD_XCELLS    17
#define BOARD_YCELLS    9

#define CELL_POWER      0x28000         /* cell is a power point */
#define CELL_IMPRISON   0x4000          /* actor is imprisoned */

/* board cells values for Archon */

#define CELL_LUMI       0x2000          /* participates in luminance cycle */
#define CELL_LIGHT      0x1000          /* cell is always light */
#define CELL_DARK       0x800           /* cell is always dark */
#define CELL_LUMI_MASK  0xF

#define LUMI_DARKEST    1               /* luminance steppings in sprite */
#define LUMI_LIGHTEST   6               /*   1=darkest  6=lightest */
#define FLOOR_DARK     0                /* steppings for light and dark */
#define FLOOR_LIGHT    7                /*   outside luminance cycle */
#define LUMI_COUNT      8               /* number of luminance states (0..7) */

/* board cells values for Adept */

#define CELL_EARTH      0x1200000       /* cell in the realm of Earth */
#define CELL_WATER      0x2200000       /* cell in the realm of Water */
#define CELL_AIR        0x3000000       /* cell in the realm of Air */
#define CELL_FIRE       0x4200000       /* cell in the realm of Fire */
#define CELL_VOID       0x5000000       /* a void cell */

#define CELL_CHAOS_CITADEL      0x6000000 /* the Citadel of Chaos */
#define CELL_ORDER_CITADEL      0x7000000 /* the Citadel of Order */

#define CELL_ELEMENT_MASK       (0x7000000 | CELL_ANIM_FLOOR)

#define CELL_ANIM_POWER 0x0A0000        /* power sprite has this many steps */
#define CELL_ANIM_FLOOR 0xF00000        /* floor sprite has this many steps */
#define CELL_ANIM_MASK  (CELL_ANIM_FLOOR | CELL_ANIM_POWER)
#define CELL_ANIM_SHIFT_POWER 16        /* bitshift needed to get the number */
#define CELL_ANIM_SHIFT_FLOOR 20        /* bitshift needed to get the number */

/*--------------------------------------------------------------------------*/
/* spells                                                                   */
/*--------------------------------------------------------------------------*/

enum {
  SPELL_TELEPORT = 1,
  SPELL_HEAL,
  SPELL_SHIFT_TIME,
  SPELL_EXCHANGE,
  SPELL_SUMMON_ELEMENTAL,
  SPELL_REVIVE,
  SPELL_IMPRISON,
  SPELL_CEASE_CONJURING,

  SPELL_FIRST = SPELL_TELEPORT,
  SPELL_LAST  = SPELL_CEASE_CONJURING,
  SPELL_COUNT = (SPELL_LAST - SPELL_FIRST + 1),
  SPELL_COUNT_2 = SPELL_COUNT + 2
};

/*--------------------------------------------------------------------------*/
/* rules result codes / message codes                                       */
/*--------------------------------------------------------------------------*/

enum {
  BOARD_NO_MOVE = 0,
  BOARD_NOT_YOURS,
  BOARD_IMPRISONED,
  BOARD_CANT_MOVE,
  BOARD_MOVED_LIMIT,
  BOARD_OCCUPIED,
  BOARD_OPPOSED,
  BOARD_OUT_OF_RANGE,
  BOARD_CANT_CAST_SPELLS,
  BOARD_SPELL_UNAVAILABLE,
  BOARD_SPELL_NO_SOURCES,
  BOARD_SPELL_NO_TARGETS,
  BOARD_EMPTY_SPELL_SOURCE,
  BOARD_EMPTY_SPELL_TARGET,
  BOARD_POWER_POINT,
  BOARD_CANCEL,
  BOARD_NEED_SOURCE,
  BOARD_NEED_TARGET,
  BOARD_NEED_FIELD,
  BOARD_NEED_SPELL,
  BOARD_NEED_SPELL_SOURCE,
  BOARD_NEED_SPELL_TARGET,
  BOARD_STEP_OK,
  BOARD_MOVE_OK,
  BOARD_FIELD_ATTACKER_WIN,
  BOARD_FIELD_DEFENDER_WIN,
  BOARD_FIELD_BOTH_LOSE,
  BOARD_GAME_OVER,
  BOARD_OK_FIRST = BOARD_NEED_SOURCE
};

#define BOARD_RESULT_OK(x) ((x) >= BOARD_OK_FIRST)

enum {
  WINNER_NOT_YET = 0,
  WINNER_DARK,
  WINNER_LIGHT,
  WINNER_TIE,
};


#endif /* __MY_BOARD_CONST_H */
