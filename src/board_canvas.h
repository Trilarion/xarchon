/*--------------------------------------------------------------------------*/
/* game board canvas                                                        */
/*--------------------------------------------------------------------------*/

#ifndef __MY_BOARD_CANVAS_H
#define __MY_BOARD_CANVAS_H

#include "actors.h"

/*--------------------------------------------------------------------------*/
/* defines                                                                  */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* structures                                                               */
/*--------------------------------------------------------------------------*/

typedef struct {                        /* Slow Sprite Paint */
  int count;
  int sx1, sy1;
  int both;                            /* double ssp -- for exchange spell */
  int sx2, sy2;
  void * sprite1;
  void * sprite2;
  int complete;                        /* ssp completed */
} SSP;

typedef struct {
  void *ptr;
  long fg, bg;
} FONT;

typedef struct {
  SSP ssp;                             /* Slow Sprite Paint data */
  FONT font;
  char message[64];                    /* last board message displayed */
  int any_output;                      /* if any output emitted */
  int width, height;                   /* # of pixels wide/high board is */
  int corner_x, corner_y;              /* coordinates of top left corner */
} BOARD_CANVAS;

/*--------------------------------------------------------------------------*/
/* function                                                                 */
/*--------------------------------------------------------------------------*/

void board_canvas_init(BOARD_CANVAS *canvas);

int board_canvas_pix_x(BOARD_CANVAS *canvas, int x);
int board_canvas_pix_y(BOARD_CANVAS *canvas, int y);
int board_canvas_revive_x_origin(BOARD_CANVAS *canvas, int side);
int board_canvas_revive_y_origin(BOARD_CANVAS *canvas, int side);
int board_canvas_revive_cell_ysize(BOARD_CANVAS *canvas);

void board_canvas_start_game(BOARD_CANVAS *canvas, int cell_w, int cell_h);
void board_canvas_start_turn(BOARD_CANVAS *canvas, int side);
void board_canvas_ssp_reset(BOARD_CANVAS *canvas);
void board_canvas_ssp_init(BOARD_CANVAS *canvas, 
			   int x1, int y1, int x2, int y2, 
			   void *sprite1, void *sprite2, int both);
int board_canvas_ssp_frame(BOARD_CANVAS *canvas);
int board_canvas_ssp_done(BOARD_CANVAS *canvas);

void board_canvas_refresh(BOARD_CANVAS *canvas);
void board_canvas_paint_cell(BOARD_CANVAS *canvas, int sx, int sy, int flags,
			    ACTOR * actor, int lumi, int anim_frame);
void board_canvas_paint_actor(BOARD_CANVAS *canvas, int sx, int sy, 
			      ACTOR * actor, int dir);
void board_canvas_paint_revive_option(BOARD_CANVAS *canvas, 
				      int actor_num, int side, int i);
void board_canvas_paint_cursor_box(BOARD_CANVAS *canvas, 
				   int sx, int sy, int side);
void board_canvas_erase_cursor_box(BOARD_CANVAS *canvas, 
				   int sx, int sy);
void board_canvas_message(BOARD_CANVAS *canvas, int lumi, int lumi_d,
			  char *msg);
     

#endif /* __MY_BOARD_CANVAS_H */
