/*--------------------------------------------------------------------------*/
/* game board canvas                                                        */
/*--------------------------------------------------------------------------*/

#include <config.h>
/* uncomment whatever includes are needed
   #include <stdarg.h>
*/   
#include <stdio.h>
/*
  #include <stdlib.h>
  #include <string.h>
*/

#include "main.h"
#include "sprite.h"
#include "canvas.h"
#include "board_const.h"
#include "board_canvas.h"

/*--------------------------------------------------------------------------*/
/* defines                                                                  */
/*--------------------------------------------------------------------------*/

#define FONT_NAME      "-misc-fixed-medium-*-normal-*-15-0-*-*-*-*-iso8859-1"
#define V_CELL_YSIZE   40               /* revive cell height */

/*--------------------------------------------------------------------------*/
/* board_canvas_init                                                        */
/*--------------------------------------------------------------------------*/

/* sort of a constructor -          */
/* must be called before start_game */

void board_canvas_init(BOARD_CANVAS *canvas)
{
  canvas->font.ptr = NULL;
  /* is this correct syntax? */
  memset(canvas->message, 0, sizeof(canvas->message)); 
  canvas->any_output = 0;
  canvas->width = canvas->height = 0;
  canvas->corner_x = canvas->corner_y = 0;
}

/*--------------------------------------------------------------------------*/
/* board_canvas_pix_x                                                       */
/*--------------------------------------------------------------------------*/

int board_canvas_pix_x(BOARD_CANVAS *canvas, int x)
{
  return canvas->corner_x + CELL_X(x);
}

/*--------------------------------------------------------------------------*/
/* board_canvas_pix_y                                                       */
/*--------------------------------------------------------------------------*/

int board_canvas_pix_y(BOARD_CANVAS *canvas, int y)
{
  return canvas->corner_y + CELL_Y(y);
}

/*--------------------------------------------------------------------------*/
/* board_canvas_revive_x_origin                                             */
/*--------------------------------------------------------------------------*/

int board_canvas_revive_x_origin(BOARD_CANVAS *canvas, int side)
{
  if (side == 0) /* light, on the left (?) */
    return CELL_XSIZE;
  else
    return CANVAS_WIDTH - CELL_XSIZE * 2;
}

/*--------------------------------------------------------------------------*/
/* board_canvas_revive_y_origin                                             */
/*--------------------------------------------------------------------------*/

int board_canvas_revive_y_origin(BOARD_CANVAS *canvas, int side)
{
  return CELL_YSIZE;
}

/*--------------------------------------------------------------------------*/
/* board_canvas_revive_cell_ysize                                           */
/*--------------------------------------------------------------------------*/

int board_canvas_revive_cell_ysize(BOARD_CANVAS *canvas)
{
  return V_CELL_YSIZE;
}

/*--------------------------------------------------------------------------*/
/* board_canvas_start_game                                                  */
/*--------------------------------------------------------------------------*/

void board_canvas_start_game(BOARD_CANVAS *canvas, int cell_w, int cell_h)
{
  /* setup font stuff */
  if (canvas->font.ptr == NULL) {
    canvas->font.ptr = canvas_font_load(FONT_NAME);
    canvas->font.fg = canvas_alloc_color(255, 255, 0);
    canvas->font.bg = canvas_alloc_color(255, 0, 0);
  }

  canvas->corner_x = CELL_X((CANVAS_XCELLS - cell_w)/2.0);
  canvas->corner_y = CELL_Y((CANVAS_YCELLS - cell_h)/2.0);
  /* I don't think width and height are ever used - mhc 2001.10.26 */
  canvas->width = CELL_X(cell_w);
  canvas->height = CELL_Y(cell_h);

  canvas->any_output = 0;
}

/*--------------------------------------------------------------------------*/
/* board_canvas_start_turn                                                  */
/*--------------------------------------------------------------------------*/

void board_canvas_start_turn(BOARD_CANVAS *canvas, int side)
{
  board_canvas_ssp_reset(canvas);
  board_canvas_refresh(canvas);
}

/*--------------------------------------------------------------------------*/
/* board_canvas_ssp_reset                                                   */
/*--------------------------------------------------------------------------*/

/* must call this at the beginning of each turn */

void board_canvas_ssp_reset(BOARD_CANVAS *canvas)
{
  canvas->ssp.complete = 1;
  canvas->ssp.count = 0;
  canvas->ssp.sx1 = 0;
  canvas->ssp.sy1 = 0;
  canvas->ssp.sx2 = 0;
  canvas->ssp.sy2 = 0;
  canvas->ssp.sprite1 = NULL;
  canvas->ssp.sprite2 = NULL;
  canvas->ssp.both = 0;
}

/*--------------------------------------------------------------------------*/
/* board_canvas_ssp_init                                                    */
/*--------------------------------------------------------------------------*/

void board_canvas_ssp_init(BOARD_CANVAS *canvas, 
			   int x1, int y1, int x2, int y2, 
			   void *sprite1, void *sprite2, int both)
{
  canvas->ssp.complete = 0;
  canvas->ssp.count = 0;
  canvas->ssp.sx1 = x1;
  canvas->ssp.sy1 = y1;
  canvas->ssp.sx2 = x2;
  canvas->ssp.sy2 = y2;
  canvas->ssp.sprite1 = sprite1;
  canvas->ssp.sprite2 = sprite2;
  canvas->ssp.both = both;
  
  /* if (x1 != x2 || y1 != y2)
     board_paint_cell(x2, y2); */         /* repaint cell to remove cursor */
}

/*--------------------------------------------------------------------------*/
/* board_canvas_ssp_frame                                                   */
/*--------------------------------------------------------------------------*/

int board_canvas_ssp_frame(BOARD_CANVAS *canvas)
{
  if (canvas->ssp.count == CELL_YSIZE) {
    canvas->ssp.complete = 1;
    return 0;
  }

  canvas->ssp.count++;
  return 1;
}

/*--------------------------------------------------------------------------*/
/* board_canvas_ssp_done                                                    */
/*--------------------------------------------------------------------------*/

int board_canvas_ssp_done(BOARD_CANVAS *canvas)
{
  return canvas->ssp.complete == 1;
}

/*--------------------------------------------------------------------------*/
/* board_canvas_refresh                                                     */
/*--------------------------------------------------------------------------*/

void board_canvas_refresh(BOARD_CANVAS *canvas)
{
  if (canvas->any_output) {
    canvas_refresh();
    canvas->any_output = 0;
  }
}

/*--------------------------------------------------------------------------*/
/* board_canvas_paint_cell                                                  */
/*--------------------------------------------------------------------------*/

void board_canvas_paint_cell(BOARD_CANVAS *canvas, int x, int y, int flags,
			     ACTOR * actor, int lumi, int anim_frame)
{
  int ssp = 0;
  void *other = NULL;
  int state, stepping;
  int sx, sy;

  sx = board_canvas_pix_x(canvas, x);
  sy = board_canvas_pix_y(canvas, y);

  /* floor_sprite appears to be a global from actors.c. */

  if (flags & CELL_ANIM_FLOOR)
    stepping = anim_frame %
      ((flags & CELL_ANIM_FLOOR)>>CELL_ANIM_SHIFT_FLOOR);
  else
    stepping = 0;
  switch (flags & CELL_ELEMENT_MASK) {
  case CELL_EARTH:
    state = STATE_EARTH_ELEM;
    break;
  case CELL_WATER:
    state = STATE_WATER_ELEM;
    break;
  case CELL_AIR:
    state = STATE_AIR_ELEM;
    break;
  case CELL_FIRE:
    state = STATE_FIRE_ELEM;
    break;
  case CELL_VOID:
    state = STATE_EMPTY;
    break;
  default:
    /* Archon board */
    state = STATE_BOARD;
    stepping = (flags & CELL_LIGHT) ? FLOOR_LIGHT :
      (flags & CELL_DARK) ? FLOOR_DARK : lumi;
  }
  sprite_set_state(floor_sprite, state, stepping);
  sprite_paint(floor_sprite, state, sx, sy);
  if (flags & CELL_POWER) {
    sprite_set_state(floor_sprite, STATE_POWER, anim_frame %
		     ((/* flags & */ CELL_ANIM_POWER)>>CELL_ANIM_SHIFT_POWER));
    sprite_paint(floor_sprite, STATE_POWER, sx, sy);
  }
  if (canvas->ssp.count > 0) {
    if (sx == canvas->ssp.sx1 && sy == canvas->ssp.sy1)
      ssp = 1;       /* we're the cell being teleported from */
    else if (sx == canvas->ssp.sx2 && y == canvas->ssp.sy2)
      ssp = 2;       /* we're the cell being teleported to */
  }
  /* draw any creature present in this cell... */
  if (actor != NULL) {
    if (ssp == 0 || (ssp == 2 && !canvas->ssp.both))
      /* if there's nothing fancy going on, or if this creature is being */
      /* teleported upon, just draw it normally */
      sprite_paint(actor->sprite, SPRITE_STOP, sx, sy);
    else
      /* else this creature is teleporting out of the cell - draw only */
      /* the lower portion */
      sprite_paint_clipped(actor->sprite, SPRITE_STOP,
			   sx, sy + canvas->ssp.count, 0, canvas->ssp.count,
			   CELL_XSIZE, CELL_YSIZE - canvas->ssp.count);
  }
  if (ssp == 2)
    other = canvas->ssp.sprite1;
  else if (ssp == 1 && canvas->ssp.both)
    other = canvas->ssp.sprite2;
  if (other)
    /* something is teleporting in to this square - draw its upper portion */
    sprite_paint_clipped(other, SPRITE_STOP, sx, sy, 0, 0,
			 CELL_XSIZE, canvas->ssp.count);
  canvas->any_output = 1;
}

/*--------------------------------------------------------------------------*/
/* board_canvas_paint_actor                                                 */
/*--------------------------------------------------------------------------*/

void board_canvas_paint_actor(BOARD_CANVAS *canvas, int sx, int sy, 
			      ACTOR * actor, int dir)
{
  if (actor_is_elem(actor)) {
    sprite_paint(actor->sprite, sprite_get_state(actor->sprite), sx, sy);
  } else {
    sprite_paint(actor->sprite,
		 ( (CELL_XOFFSET(sx) == 0 && CELL_YOFFSET(sy) == 0)
		   ? SPRITE_STOP : dir ), sx, sy);
  }
  canvas->any_output = 1;
}

/*--------------------------------------------------------------------------*/
/* board_canvas_paint_revive_option                                         */
/*--------------------------------------------------------------------------*/

void board_canvas_paint_revive_option(BOARD_CANVAS *canvas, 
				      int actor_num, int side, int i)
{
  int x, y_min, y_step, facing;

  /* we infer facing from the side.                               */
  /* - this is not how actor are normally set. Can that be fixed? */
  if (side == 0) /* light side */ {
    facing = STATE_MOVE_RIGHT;
  } else {
    facing = STATE_MOVE_LEFT;
  }
  
  x = board_canvas_revive_x_origin(canvas, side);
  y_min = board_canvas_revive_y_origin(canvas, side);
  y_step = board_canvas_revive_cell_ysize(canvas);

  sprite_paint(actors_list[actor_num].sprite, facing, x, i * y_step + y_min);
  canvas->any_output = 1;
}

/*--------------------------------------------------------------------------*/
/* board_canvas_paint_cursor_box                                            */
/*--------------------------------------------------------------------------*/

void board_canvas_paint_cursor_box(BOARD_CANVAS *canvas, 
				   int sx, int sy, int side)
{
  /* cursor_sprite is a global from actors.h */
  sprite_set_state(cursor_sprite, STATE_BOARD, side);
  sprite_paint(cursor_sprite, STATE_BOARD, sx, sy);

  canvas->any_output = 1;
}

/*--------------------------------------------------------------------------*/
/* board_canvas_erase_cursor_box                                            */
/*--------------------------------------------------------------------------*/

void board_canvas_erase_cursor_box(BOARD_CANVAS *canvas, 
				   int sx, int sy)
{
  /* cursor_sprite is a global from actors.h */
  sprite_set_state(cursor_sprite, STATE_BOARD, 1); 
  sprite_paint(cursor_sprite, STATE_BOARD, sx, sy);

  canvas->any_output = 1;
}

/*--------------------------------------------------------------------------*/
/* board_canvas_message                                                     */
/*--------------------------------------------------------------------------*/

void board_canvas_message(BOARD_CANVAS *canvas, int lumi, int lumi_d,
			  char *msg)
{
  char lumi_msg[64];
  int width, height;

  if (msg != NULL) {
    if (strcmp(msg, canvas->message) == 0)
      return;
  } else
    strcpy(msg, canvas->message);

  canvas_font_size(msg, canvas->font.ptr, &width, &height);
  canvas_rectangle(0, CANVAS_HEIGHT - CELL_YSIZE,
		   CANVAS_WIDTH, CELL_YSIZE, canvas->font.bg);
  canvas_font_print(msg, (CANVAS_WIDTH - width) / 2, CANVAS_HEIGHT - height,
		    canvas->font.ptr, canvas->font.fg);
  strcpy(canvas->message, msg);

  sprintf(lumi_msg, "(%c%c%d)", (lumi_d == -1 ? '-' : '+'), 
	  (lumi <= 3 ? 'D' : 'L'), lumi);
  canvas_font_size(lumi_msg, canvas->font.ptr, &width, &height);
  canvas_font_print(lumi_msg, CANVAS_WIDTH - width, CANVAS_HEIGHT - height,
		    canvas->font.ptr, canvas->font.fg);

  canvas->any_output = 1;

#ifdef AUTOPILOT
  printf("board:  %s\n", canvas->message);
#endif
}
