/*--------------------------------------------------------------------------*/
/* interface                                                                */
/*--------------------------------------------------------------------------*/

#include <config.h>

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "actors.h"
#include "main.h"
#include "board.h"
#include "iface.h"
#include "human.h"
#include "computer.h"
#include "network.h"

/*--------------------------------------------------------------------------*/
/* defines                                                                  */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* structures                                                               */
/*--------------------------------------------------------------------------*/

typedef struct {
   int num;
   int iface;
   int mode;
   void (*turn_func)(int side_num, int mode);
   void (*frame_func)(int *keys_down);
} SIDE;

/*--------------------------------------------------------------------------*/
/* variables                                                                */
/*--------------------------------------------------------------------------*/

static IFACE_CONFIG iface_config;

SIDE sides[2] = { {}, {} };
SIDE *side;

static int keys_down[STATE_MOVE_COUNT];

/*--------------------------------------------------------------------------*/
/* iface_start                                                              */
/*--------------------------------------------------------------------------*/

char *iface_start(BOARD_DATA *board, int *light_first)
{
   IFACE_PLAYER *player, *players[2];
   int i;

   players[0] = players[1] = NULL;
   for (player = list_head(&iface_config.players); player != NULL; player = list_next(player)) {
      if (player->name == NULL)
         continue;
      if (strcmp(player->name, iface_config.light_name) == 0)
         players[0] = player;
      if (strcmp(player->name, iface_config.dark_name) == 0)
         players[1] = player;
   }
   if (players[0] == NULL || players[1] == NULL)
      return "cannot start:  no players specified";

   if (players[0]->type == IFACE_NETWORK && players[1]->type == IFACE_NETWORK)
      return "cannot start:  two network players selected";

   for (i = 0; i < 2; i++) {
      side = &sides[i];
      side->num = i;
      side->iface = players[i]->type;
      if (side->iface == IFACE_HUMAN) {
         human_start(i, players[i]->human, board);
         side->turn_func = human_turn;
         side->frame_func = human_frame;
      } else if (side->iface == IFACE_COMPUTER) {
         computer_start(i, players[i]->computer, board);
         side->turn_func = computer_turn;
         side->frame_func = computer_frame;
      } else if (side->iface == IFACE_NETWORK) {
         iface_config.light_first = 1;  /* force light starts */
         if (!network_start(i, players[i]->network, board))
            return "cannot start:  network player not connected";
         side->turn_func = network_turn;
         side->frame_func = network_frame;
      }
   }

   *light_first = iface_config.light_first;
   return NULL;
}

/*--------------------------------------------------------------------------*/
/* iface_turn                                                               */
/*--------------------------------------------------------------------------*/

void iface_turn(int side_num, int mode)
{
   side = &sides[side_num];
   side->mode = mode;
   side->turn_func(side_num, mode);
}

/*--------------------------------------------------------------------------*/
/* iface_frame                                                              */
/*--------------------------------------------------------------------------*/

void iface_frame(void)
{
   int other;

   side->frame_func(keys_down);

   other = !side->num;
   if (sides[other].iface == IFACE_NETWORK) {
      sides[other].turn_func(side->num, 0);
      sides[other].frame_func(keys_down);
   }
}

/*--------------------------------------------------------------------------*/
/* iface_key_down                                                           */
/*--------------------------------------------------------------------------*/

int iface_key_down(int key)
{
   return keys_down[key];
}

/*--------------------------------------------------------------------------*/
/* iface_notify_computer                                                    */
/*--------------------------------------------------------------------------*/

void iface_notify_computer(int mode)
{
   if (sides[0].iface == IFACE_COMPUTER)
      iface_turn(0, mode);
   if (sides[1].iface == IFACE_COMPUTER)
      iface_turn(1, mode);
}

/*--------------------------------------------------------------------------*/
/* iface_is_pausable                                                        */
/*--------------------------------------------------------------------------*/

int iface_is_pausable(void)
{
   return (sides[0].iface != IFACE_NETWORK &&
           sides[1].iface != IFACE_NETWORK);
}

/*--------------------------------------------------------------------------*/
/* iface_get_config                                                         */
/*--------------------------------------------------------------------------*/

IFACE_CONFIG *iface_get_config(void)
{
   return &iface_config;
}

/*--------------------------------------------------------------------------*/
/* iface_new_player                                                         */
/*--------------------------------------------------------------------------*/

IFACE_PLAYER *iface_new_player(void)
{
   IFACE_PLAYER *player;
   
   player = list_insert_after(
      &iface_config.players, NULL, sizeof(IFACE_PLAYER));
   player->human = calloc(1, sizeof(HUMAN_CONFIG));
   player->computer = calloc(1, sizeof(COMPUTER_CONFIG));
   player->network = calloc(1, sizeof(NETWORK_CONFIG));

   return player;
}

/*--------------------------------------------------------------------------*/
/* iface_delete_player                                                      */
/*--------------------------------------------------------------------------*/

void iface_delete_player(IFACE_PLAYER *player)
{
   if (!strcmp(iface_config.light_name, player->name))
      strcpy(iface_config.light_name, "light?");
   if (!strcmp(iface_config.dark_name, player->name))
      strcpy(iface_config.dark_name, "dark?");
   
   list_delete(&iface_config.players, player);
}

/*--------------------------------------------------------------------------*/
/* iface_config_read                                                        */
/*--------------------------------------------------------------------------*/

void iface_config_read(FILE *fp)
{
   int i, num;
   IFACE_PLAYER *player;

   fscanf(fp, "%32s %32s %d %d",
          iface_config.light_name, iface_config.dark_name,
          &iface_config.light_first, &num);
   list_create(&iface_config.players);
   for (i = 0; i < num; i++) {
      player = iface_new_player();
      fscanf(fp, "%32s %d", player->name, &player->type);
      human_config_read(fp, (HUMAN_CONFIG *)player->human);
      computer_config_read(fp, (COMPUTER_CONFIG *)player->computer);
      network_config_read(fp, (NETWORK_CONFIG *)player->network);

#ifdef AUTOPILOT
      if (i == 2) {
         printf("iface:  autopilot mode:  use only 2 players\n");
         exit(EXIT_FAILURE);
      }
      if (player->type != IFACE_COMPUTER) {
         printf("iface:  autopilot mode:  use only computer players\n");
         exit(EXIT_FAILURE);
      }
      if (strcmp(iface_config.light_name, iface_config.dark_name) == 0) {
         printf("iface:  autopilot mode:  use different players for light and dark\n");
         exit(EXIT_FAILURE);
      }
#endif
   }
}

/*--------------------------------------------------------------------------*/
/* iface_config_write                                                       */
/*--------------------------------------------------------------------------*/

void iface_config_write(FILE *fp)
{
   IFACE_PLAYER *player;

   fprintf(fp, "%-32s %-32s\n%d\n%d\n\n",
           iface_config.light_name, iface_config.dark_name,
           iface_config.light_first,
           list_count(&iface_config.players));

   for (player = list_head(&iface_config.players); player != NULL; player = list_next(player)) {
      fprintf(fp, "\n%-32s\n%d\n", player->name, player->type);
      human_config_write(fp, (HUMAN_CONFIG *)player->human);
      fprintf(fp, "\n");
      computer_config_write(fp, (COMPUTER_CONFIG *)player->computer);
      fprintf(fp, "\n");
      network_config_write(fp, (NETWORK_CONFIG *)player->network);
      fprintf(fp, "\n");
   }
}
