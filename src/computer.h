/*--------------------------------------------------------------------------*/
/* computer                                                                 */
/*--------------------------------------------------------------------------*/

#ifndef __MY_COMPUTER_H
#define __MY_COMPUTER_H

#include <stdio.h>

#include "actors.h"
#include "iface.h"

/*--------------------------------------------------------------------------*/
/* structures                                                               */
/*--------------------------------------------------------------------------*/

typedef struct {
   char rules[33];                      /* computer rules file */
  /* SCRAP THE OLD BOARD MODE. THIS PARAMETER WILL BE IGNORED. */
  /* - mhc 2001.10.26 */
   int old_board_mode;                  /* board is AI- or rule- based */
   int data_num;                        /* which data structure to use */
} COMPUTER_CONFIG;

/* this is how I think players should work (human & network too) */
/* but for now, this struct is unused - see static variables in computer.c */
/* - mhc 2001.11.02 */

/* 
   typedef struct {
   BOARD_DATA *board;
   COMPUTER_CONFIG *config;
   MOVE good_moves[33];
   COMMAND *cmd;
   int side;
   } COMPUTER_PLAYER;
*/

/*--------------------------------------------------------------------------*/
/* public functions                                                         */
/*--------------------------------------------------------------------------*/

void computer_start(int side, COMPUTER_CONFIG *config, BOARD_DATA *main_board);
/* need side info for board_frame. */
void computer_frame(int * keys_down);
void computer_turn(int side, int mode);
void computer_config_read(FILE *fp, COMPUTER_CONFIG *config);
void computer_config_write(FILE *fp, COMPUTER_CONFIG *config);
void computer_config_edit(COMPUTER_CONFIG *_config);
int computer_field_score(ACTOR *attacker, ACTOR *defender,
                         int *attacker_damage, int *defender_damage,
                         int *attacker_hits, int *defender_hits);

#endif
